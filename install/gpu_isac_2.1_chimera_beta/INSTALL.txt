# Untar the tar archive
tar -xf GPU_ISAC_CHIMERA.tar

# Load a sphire module
module load sphire

# Put CUDA bin and lib in your PATH/LD_LIBRARY_PATH
export PATH=/path/to/cuda/bin:${PATH}
export LD_LIBRARY_PATH=/path/to/cuda/lib64:${LD_LIBRARY_PATH}

# Compile the cuda library
cd vChimera/cuda
nvcc gpu_aln_common.cu gpu_aln_noref.cu -o gpu_aln_pack.so -shared -Xcompiler -fPIC -lcufft -std=c++11

# Adjust librarys to work with the correct cuda librarys
cd ../eman2/sparx/libpy
sed -i.bkp "s|/home/schoenf/work/code/cuISAC/cuda|$(realpath ../../../cuda)|g" applications.py
sed -i.bkp2 's|statistics.sum_oe( data, "a", CTF, EMData(), myid=myid|statistics.sum_oe( data, "a", CTF, EMData()|g' applications.py

# Use the correct libraries within GPU ISAC and the correct environment
cd ../bin
ln -rs ../libpy/* .
sed -i.bkp "s|/home/schoenf/applications/sphire/v1.1/envs/sphire_1.3/bin|$(dirname $(which sphire))|g" sxisac2_gpu.py
sed -i.bkp2 "s/^\(.*options, args.*\)$/\1\n    os.environ['CUDA_VISIBLE_DEVICES'] = options.gpu_devices\n    options.gpu_devices = ','.join(map(str, range(len(options.gpu_devices.split(',')))))/g" sxisac2_gpu.py

# Example usage:
This will run gpu_isac on gpu 
mpirun -np 6 /pat/to/sxisac2_gpu.py bdb:path/to/stack  out_dir  --CTF  --radius  160  --target_radius  29  --target_nx  76  --img_per_grp  100  --minimum_grp_size  60  --thld_err  0.7  --center_method  0  --gpu_devices  1,3
